#!/usr/bin/python3
import os
import argparse
import time

import dns.query
import dns.rdatatype
import dns.tsigkeyring
import dns.update
import dns.rcode

import json

path = os.path.dirname(os.path.abspath(__file__))

if __name__ =='__main__':
    parser = argparse.ArgumentParser(description="Authenticator for certbot")
    parser.add_argument('-c', '--config', default='certbot.json')
    args = parser.parse_args()

    with open(os.path.join(path,args.config)) as file:
        config = json.load(file)

    domain = os.getenv("CERTBOT_DOMAIN")
    validation = os.getenv("CERTBOT_VALIDATION")
    query_zone = config[domain]['zone']

    keyring = dns.tsigkeyring.from_text({
            config[domain]['key']['name']: config[domain]['key']['secret'],
        })

    update = dns.update.Update(
            query_zone,
            keyring=keyring,
            keyalgorithm=config[domain]['key']['algorithm']
        )

    update.add('', 300, dns.rdatatype.TXT, validation)
    response = dns.query.tcp(update, config[domain]['server'], 45, config[domain]['port'])
    time.sleep(10)
    rcode = response.rcode()
    if rcode != dns.rcode.NOERROR:
        print(rcode)
        sys.exit(1)
